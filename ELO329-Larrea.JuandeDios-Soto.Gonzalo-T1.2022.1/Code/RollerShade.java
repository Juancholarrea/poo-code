
/**
 *
 * TODO_javadoc.
 *
 * @version 1.0 (01/04/2022).
 *
 */
public class RollerShade extends DomoticDevice {

    /*******************************************************************************************************
     * RollerShade
     *******************************************************************************************************/
    /**
     *
     * TODO_javadoc.
     *
     * @param id TODO_javadoc.
     * @param alpha TODO_javadoc.
     * @param max_length TODO_javadoc.
     * @param channel TODO_javadoc.
     *
     */
    public RollerShade(int id, double alpha, double max_length, int channel) {

        super(id, channel);

        this.max_length = max_length;

        motor = new Motor(alpha);
    }

    /*******************************************************************************************************
     * startUp
     *******************************************************************************************************/
    /**
     *
     * TODO_javadoc.
     *
     */
    public void startUp() {
        motor.turnUp();
    }

    /*******************************************************************************************************
     * startDown
     *******************************************************************************************************/
    /**
     *
     * TODO_javadoc.
     *
     */
    public void startDown() {
        motor.turnDown();
    }

    /*******************************************************************************************************
     * stop
     *******************************************************************************************************/
    /**
     *
     * TODO_javadoc.
     *
     */
    public void stop() {
        motor.stop();
    }

    /*******************************************************************************************************
     * advanceTime
     *******************************************************************************************************/
    /**
     *
     * TODO_javadoc.
     *
     * @param delta TODO_javadoc.
     *
     */
    public void advanceTime(double delta) {
        motor.advanceTime(delta);
    }

    /*******************************************************************************************************
     * getHeader
     *******************************************************************************************************/
    /**
     *
     * TODO_javadoc.
     *
     * @return TODO_javadoc.
     *
     */
    public String getHeader() {
        return "RS" + getId();
    }

    /*******************************************************************************************************
     * toString
     *******************************************************************************************************/
    /**
     *
     * TODO_javadoc.
     *
     * @return TODO_javadoc.
     *
     */
    public String toString() {
        return String.valueOf(Math.round(length / max_length * 100.0D));
    }
    /**
     *
     * TODO_javadoc.
     *
     * @version 1.0 (01/04/2022).
     *
     */
    private class Motor {


        /** TODO_javadoc. */
        private static final double RADIUS = 0.04D;

        /** TODO_javadoc. */
        private double alpha;

        /** TODO_javadoc. */
        private MotorState state = MotorState.STOPPED;

        /*******************************************************************************************************
         * Motor
         *******************************************************************************************************/
        /**
         *
         * TODO_javadoc.
         *
         * @param alpha TODO_javadoc.
         *
         */
        public Motor(double alpha) {
            this.alpha = alpha;
        }

        /*******************************************************************************************************
         * turnUp
         *******************************************************************************************************/
        /**
         *
         * TODO_javadoc.
         *
         */
        public void turnUp() {
            state = MotorState.UPWARD;
        }

        /*******************************************************************************************************
         * turnDown
         *******************************************************************************************************/
        /**
         *
         * TODO_javadoc.
         *
         */
        public void turnDown() {
            state = MotorState.DOWNWARD;
        }

        /*******************************************************************************************************
         * stop
         *******************************************************************************************************/
        /**
         *
         * TODO_javadoc.
         *
         */
        public void stop() {
            state = MotorState.STOPPED;
        }

        /*******************************************************************************************************
         * advanceTime
         *******************************************************************************************************/
        /**
         *
         * TODO_javadoc.
         *
         * @param delta TODO_javadoc.
         *
         */
        public void advanceTime(double delta) {

            double increment = alpha * delta * RADIUS;

            switch (state) {

            case STOPPED :

                break;

            case DOWNWARD :

                if (length + increment <= max_length) {
                    length += increment;
                }
                else {
                    length = max_length;
                }

                break;

            case UPWARD :

                if (length - increment >= 0.0D) {
                    length -= increment;
                }
                else {
                    length = 0.0D;
                }

                break;
            }
        }
    }
    /** TODO_javadoc. */
    public static enum MotorState { UPWARD, STOPPED, DOWNWARD };

    /** TODO_javadoc. */
    //private static int nextId = 0;

    /** TODO_javadoc. */
    private double max_length;

    /** TODO_javadoc. */
    private double length = 0.0D; // cada cortina parte talmente enrollada

    /** TODO_javadoc. */
    private Motor motor = null;
}
