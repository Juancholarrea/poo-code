
/**
 *
 * TODO_javadoc.
 *
 * @version 1.0 (01/04/2022).
 *
 */
public abstract class DomoticDevice {


    /*******************************************************************************************************
     * DomoticDevice
     *******************************************************************************************************/
    /**
     *
     * TODO_javadoc.
     *
     * @param id TODO_javadoc.
     * @param channel TODO_javadoc.
     *
     */
    public DomoticDevice(int id, int channel) {

        this.id      = id;
        this.channel = channel;
    }

    /*******************************************************************************************************
     * getChannel
     *******************************************************************************************************/
    /**
     *
     * TODO_javadoc.
     *
     * @return TODO_javadoc.
     *
     */
    public int getChannel() {
        return channel;
    }

    /*******************************************************************************************************
     * getId
     *******************************************************************************************************/
    /**
     *
     * TODO_javadoc.
     *
     * @return TODO_javadoc.
     *
     */
    public int getId() {
        return id;
    }

    /*******************************************************************************************************
     * getHeader
     *******************************************************************************************************/
    /**
     *
     * TODO_javadoc.
     *
     * @return TODO_javadoc.
     *
     */
    public abstract String getHeader();


    /** TODO_javadoc. */
    protected final int id;

    /** TODO_javadoc. */
    protected int channel;
}
