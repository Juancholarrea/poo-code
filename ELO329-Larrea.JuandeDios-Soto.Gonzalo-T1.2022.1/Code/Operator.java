
import java.io.PrintStream;

import java.text.DecimalFormat;

import java.util.ArrayList;
import java.util.Scanner;


/**
 *
 * TODO_javadoc.
 *
 * @version 1.0 (01/04/2022).
 *
 */
public class Operator {


    /*******************************************************************************************************
     * Operator
     *******************************************************************************************************/
    /**
     *
     * TODO_javadoc.
     *
     * @param rollerShadeControls TODO_javadoc.
     * @param cloud TODO_javadoc.
     *
     */
    public Operator(ArrayList<RollerShadeControl> rollerShadeControls, ArrayList<LampControl> lampControls, Cloud cloud) {

        this.rollerShadeControls = rollerShadeControls;
        this.lampControls        = lampControls;
        this.cloud               = cloud;
    }

    /*******************************************************************************************************
     * executeCommands
     *******************************************************************************************************/
    /**
     *
     * TODO_javadoc.
     *
     * @param in TODO_javadoc.
     * @param out TODO_javadoc.
     *
     */
    public void executeCommands(Scanner in, PrintStream out) {

        out.println("Time\t" + cloud.getHeaders());

        while (in.hasNextInt()) {

            int commandTime = in.nextInt();

            while (time < commandTime) {

                out.println(form.format(time) + "\t" + cloud.getState());

                cloud.advanceTime(DELTA);

                time += DELTA;
            }

            String device  = in.next();
            String command = null;
            String color   = null;
            int    channel;
            switch (device.charAt(0)) {
            /***********************************************************************************************
             * Cortina
             ***********************************************************************************************/
            case 'C' :

                channel = in.nextInt();
                command = in.next();

                for (RollerShadeControl rollerShadeControl : rollerShadeControls) {

                    if (channel == rollerShadeControl.getChannel()) {

                        switch (command.charAt(0)) {
                        case 'D' : // Down
                            rollerShadeControl.startDown();
                            break;
                        case 'U' : // Up
                            rollerShadeControl.startUp();
                            break;
                        case 'S' : // Stop
                            rollerShadeControl.stop();
                            break;

                        default :
                            out.println("Unexpected roller shade command: " + command);
                            System.exit(-1);
                        }
                    }
                }

                break;

            /***********************************************************************************************
             * Lampara
             ***********************************************************************************************/

            case 'L' :

                channel = in.nextInt();
                color   = in.next();


                for (LampControl lampControl : lampControls) {

                    if (channel == lampControl.getChannel()) {
                        if (color.equals("N") && cloud.getPowerState()== Lamp.LampState.OFF) { //
                            lampControl.pressPower();
                        }else if (color.equals("N") && cloud.getPowerState()== Lamp.LampState.ON){}

                        else if (color.equals("F") && cloud.getPowerState()== Lamp.LampState.ON){
                            lampControl.pressPower();
                        }else if (color.equals("F") && cloud.getPowerState()== Lamp.LampState.OFF){}

                        else if (color.equals("R")) {
                            command= in.next();
                            switch (command) {

                                case "D": // Down
                                    lampControl.downIntensity(color);
                                    break;
                                case "U": // Up
                                    lampControl.upIntensity(color);
                                    break;
                                default:
                                    out.println("Unexpected lamp command Red: " + command);
                                    System.exit(-1);

                            }
                        }
                        else if (color.equals("G")) {
                            command= in.next();
                            switch (command.charAt(0)) {

                                case 'D' : // Down
                                    lampControl.upIntensity(color);
                                    break;
                                case 'U' : // Up
                                    lampControl.downIntensity(color);
                                    break;

                                default :
                                    out.println("Unexpected lamp command Green: " + command);
                                    System.exit(-1);
                            }

                        } else if (color.equals("B")) {
                            command= in.next();
                            switch (command) {
                                case "D" : // Down
                                    lampControl.downIntensity(color);
                                    break;
                                case "U" : // Up
                                    lampControl.upIntensity(color);
                                    break;

                                default :
                                    out.println("Unexpected lamp command Blue: " + command);
                                    System.exit(-1);
                            }
                        } else
                            System.out.println("Unexpected lamp command: + command");

                    }
                }
                break;

            default :

                out.println("Unexpected device: " + device);
                System.exit(-1);
            }

        }

        out.println(form.format(time) + "\t" + cloud.getState());
    }



    /** TODO_javadoc. */
    private final double DELTA = 0.1D;

    /** TODO_javadoc. */
    private final DecimalFormat form = new DecimalFormat("00.0");

    /** TODO_javadoc. */
    private double time = 0.0D;

    /** TODO_javadoc. */
    private ArrayList<RollerShadeControl> rollerShadeControls = null;

    /** TODO_javadoc. */
    private ArrayList<LampControl> lampControls = null;

    /** TODO_javadoc. */
    private Cloud cloud = null;
}
