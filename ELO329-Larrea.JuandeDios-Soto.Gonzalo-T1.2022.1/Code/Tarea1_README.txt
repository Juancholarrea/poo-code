Archivos que componen la Tarea1 - POO: 

Cloud.java DomoticDevice.java DomoticDeviceControl.java  Lamp.java LampControl.java Operator.java RollerShade.java RollerShadeControl.java DomoticaTest.java configuration.txt MAKEFILE


Pasos a seguir para compilar en cmd windows:

cd Direccion_carpeta_codigos   (ejemplo: C:\Users\juand\Desktop\Tarea1 - POO\DomoticaTest)

javac *.java

java DomoticaTest configuration.txt > ConfigurationSalida.csv


El programa practica la orientación a objetos en un sistema automatizado (Domótico) con una o más cortinas motorizadas y un tipo de lámpara.
Al ser ejecutado, utilizando el archivo configuration.txt como parámetro, la salida muestra el comportamiento de movimiento de las cortinas y la intensidad de las lámparas dependiendo de su estado. 
Todos los Stage del proyecto fueron desarrollados en conjunto y dirigidos a una única salida general.


