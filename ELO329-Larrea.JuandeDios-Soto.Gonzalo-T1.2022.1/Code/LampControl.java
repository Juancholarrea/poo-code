
/**
 *
 * TODO_javadoc.
 *
 * @version 1.0 (01/04/2022).
 *
 */
public class LampControl extends DomoticDeviceControl {

    /*******************************************************************************************************
     * LampControl
     *******************************************************************************************************/
    /**
     *
     * TODO_javadoc.
     *
     * @param channel TODO_javadoc.
     * @param cloud TODO_javadoc.
     *
     */
    public LampControl(int channel, Cloud cloud) {
        super(channel, cloud);
    }

    /*******************************************************************************************************
     * pressPower
     *******************************************************************************************************/
    /**
     *
     * TODO_javadoc.
     *
     */
    public void pressPower() {
        cloud.changeLampPowerState(channel);
    }

    /*******************************************************************************************************
     * upIntensity
     *******************************************************************************************************/
    /**
     *
     * TODO_javadoc.
     *
     * @param color TODO_javadoc.
     *
     */
    public void upIntensity(String color) {
        cloud.upLampIntensity(channel, color);
    }

    /*******************************************************************************************************
     * downIntensity
     *******************************************************************************************************/
    /**
     *
     * TODO_javadoc.
     *
     * @param color TODO_javadoc.
     *
     */
    public void downIntensity(String color) {
        cloud.downLampIntensity(channel, color);
    }


}
