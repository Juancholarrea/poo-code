
/**
 *
 * TODO_javadoc.
 *
 * @version 1.0 (01/04/2022).
 *
 */
public class DomoticDeviceControl {

    /*******************************************************************************************************
     * DomoticDeviceControl
     *******************************************************************************************************/
    /**
     *
     * TODO_javadoc.
     *
     * @param channel TODO_javadoc.
     * @param cloud TODO_javadoc.
     *
     */
    public DomoticDeviceControl(int channel, Cloud cloud) {

        this.channel = channel;
        this.cloud   = cloud;
    }

    /*******************************************************************************************************
     * getChannel
     *******************************************************************************************************/
    /**
     *
     * TODO_javadoc.
     *
     * @return TODO_javadoc.
     *
     */
    public int getChannel() {
        return channel;
    }


    /** TODO_javadoc. */
    protected int channel;

    /** TODO_javadoc. */
    protected Cloud cloud = null;
}

