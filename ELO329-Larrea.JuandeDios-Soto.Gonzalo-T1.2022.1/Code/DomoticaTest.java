
import java.io.CharArrayWriter;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;

/**
 *
 * TODO_javadoc.
 *
 * @version 1.0 (01/04/2022).
 *
 */
public class DomoticaTest {

    /*******************************************************************************************************
     * main
     *******************************************************************************************************/
    /**
     *
     * TODO_javadoc.
     *
     * @param args TODO_javadoc.
     *
     * @throws IOException TODO_javadoc.
     *
     */
    public static void main(String[] args) throws IOException {

        try {

            if (args.length != 1) {

                System.out.println("Usage: java DomoticaTest <configurationFile.txt>");

                System.exit(-1);
            }

            //

            System.out.println("configurationFile: '" + args[0] + "'");

            Scanner in = new Scanner(new File(args[0]));

            in.useLocale(Locale.ENGLISH);

            Cloud cloud = new Cloud();

            //
            // reading <#_de_cortinas> <#_de_lámparas> <#_controles_cortinas> <#_controles_lámparas>
            //

            int cantRollerShades = in.nextInt(); // #_de_cortinas

            System.out.println("cantRollerShades: " + cantRollerShades);

            int cantLamps = in.nextInt();    // #_de_lámparas

            System.out.println("cantLamps: " + cantLamps);

            int numRollerShadeControls = in.nextInt(); // #_controles_cortinas

            System.out.println("numRollerShadeControls: " + numRollerShadeControls);

            int numLampControls = in.nextInt();    // #_controles_lámparas

            System.out.println("numLampControls: " + numLampControls);

            //
            // rapidez angular [rad/s], largo de la tela y canal de la primera hasta n-ésima cortina
            //
            // read <alfa0> <length0> <canal0> ... <alfaN_1> <lengthN_1> <canalN_1>
            //

            System.out.println("----------------------------------------------------------------");

            for (int i = 0; i < cantRollerShades; i++) {

                double alpha    = in.nextDouble();
                double length   = in.nextDouble();
                int    channel  = in.nextInt();

                System.out.println("agregando RollerShade(alpha" + i + ": " + alpha + " length" + i + ": " + length + " channel" + i + ": " + channel + ") ...");

                cloud.addRollerShade(new RollerShade(i, alpha, length, channel));
            }

            //
            // canales de la primera a la L-ésima lámpara
            //

            System.out.println("----------------------------------------------------------------");

            for (int i = 0; i < cantLamps; i++) {

                int channel = in.nextInt();

                System.out.println("agregando Lamp(channel" + i + ":" + channel + ") ...");

                cloud.addLamp(new Lamp(i, channel));
            }

            //
            // canales del primer hasta el m-ésimo control de cortina.
            //

            System.out.println("----------------------------------------------------------------");

            ArrayList<RollerShadeControl> rollerShadeControls = new ArrayList<>();

            for (int i = 0; i < numRollerShadeControls; i++) {

                int channel = in.nextInt();

                System.out.println("agregando RollerShadeControl(channel" + i + ":" + channel + ") ...");

                rollerShadeControls.add(new RollerShadeControl(channel, cloud));
            }

            //
            // canales del primer hasta el k-ésimo control de lámpara.
            //

            System.out.println("----------------------------------------------------------------");

            ArrayList<LampControl> lampControls = new ArrayList<>();

            for (int i = 0; i < numLampControls; i++) {

                int channel = in.nextInt();

                System.out.println("agregando LampControl(channel" + i + ":" + channel + ") ...");

                lampControls.add(new LampControl(channel, cloud));
            }

            //

            Operator operator = new Operator(rollerShadeControls, lampControls, cloud);

            System.out.println("================================================================");

            operator.executeCommands(in, System.out);

            System.out.println("================================================================");
        }
        catch (Throwable e) {
            System.err.println(printStackTrace(e));
        }
    }

    /*******************************************************************************************************
     * printStackTrace
     *******************************************************************************************************/
    /**
     *
     * @param e TODO_javadoc
     *
     * @return TODO_javadoc
     */
    private static String printStackTrace(Throwable e) {

        CharArrayWriter caw = new CharArrayWriter();
        e.printStackTrace(new PrintWriter(caw));
        return caw.toString();
    }
}
