
import java.util.HashMap;


public class Lamp extends DomoticDevice {


    /*******************************************************************************************************
     * Lamp Constructor
     *******************************************************************************************************/
    /**
     *
     * TODO_javadoc.
     *
     * @param channel TODO_javadoc.
     *
     */
    public Lamp(int id, int channel) {

        super(id, channel);

        intensidad.put(LampColor.RED,   (short) 255);
        intensidad.put(LampColor.GREEN, (short) 255);
        intensidad.put(LampColor.BLUE,  (short) 255);

    }

    /*******************************************************************************************************
     * changePowerState
     *******************************************************************************************************/
    /**
     *
     * TODO_javadoc.
     *
     */
    public void changePowerState() {
        state = state == LampState.OFF ? LampState.ON : LampState.OFF;
    }


    /*******************************************************************************************************
     * getPowerState
     *******************************************************************************************************/
    /**
     *
     * TODO_javadoc.
     *
     */
    public LampState getPowerState(){
        return state;
    }


    /*******************************************************************************************************
     * getLampColor
     *******************************************************************************************************/
    /**
     *
     * TODO_javadoc.
     *
     * @param color TODO_javadoc.
     *
     */
    public LampColor getLampColor(String color) {

        switch (color.charAt(0)) {
        case 'R' :
            return LampColor.RED;
        case 'G' :
            return LampColor.GREEN;
        case 'B' :
            return LampColor.BLUE;
        }

        throw new RuntimeException("color '" + color + "' inexistente.");
    }

    /*******************************************************************************************************
     * upIntensity
     *******************************************************************************************************/
    /**
     *
     * TODO_javadoc.
     *
     * @param color TODO_javadoc.
     *
     */
    public void upIntensity(String color) {

        LampColor lc        = getLampColor(color);
        short     new_value = (short) (intensidad.get(lc) + DELTA);

        intensidad.put(lc, new_value > (short) 255 ? (short) 255 : new_value);
    }

    /*******************************************************************************************************
     * downIntensity
     *******************************************************************************************************/
    /**
     *
     * TODO_javadoc.
     *
     * @param color TODO_javadoc.
     *
     */
    public void downIntensity(String color) {

        LampColor lc        = getLampColor(color);
        short     new_value = (short) (intensidad.get(lc) - DELTA);

        intensidad.put(lc, new_value < (short) 0 ? (short) 0 : new_value);
    }

    /*******************************************************************************************************
     * getHeader
     *******************************************************************************************************/
    /**
     *
     * TODO_javadoc.
     *
     * @return TODO_javadoc.
     *
     */
    public String getHeader() {
        return "L" + getId() + "R\t" + "L" + getId() + "G\t" + "L" + getId() + "B";
    }

    /*******************************************************************************************************
     * toString
     *******************************************************************************************************/
    /**
     *
     * TODO_javadoc.
     *
     * @return TODO_javadoc.
     *
     */
    public String toString() {

        if (state == LampState.ON) {
            return "" + intensidad.get(LampColor.RED) + "\t" + intensidad.get(LampColor.GREEN) + "\t" + intensidad.get(LampColor.BLUE);
        }
        else {
            return "0\t0\t0";
        }
    }


    /** TODO_javadoc. */
    private final short DELTA = (short) 5;

    /** TODO_javadoc. */
    private HashMap<LampColor, Short> intensidad = new HashMap<>();

    /** TODO_javadoc. */
    private LampState state = LampState.OFF;

    /** TODO_javadoc. */
    private static enum LampColor { RED, GREEN, BLUE };

    /** TODO_javadoc. */
    public static enum LampState { ON, OFF };

}
