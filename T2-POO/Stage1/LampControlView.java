import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

public class LampControlView extends VBox{
    public LampControlView(LampControl lampControl) {
        VBox vbox = new VBox();
        vbox.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                if (keyEvent.getCode()==KeyCode.SPACE){
                    if (lampControl.getLampState()==LampState.ON){
                        lampControl.pressPower(channel);
                        lampControl.changeRGB(channel,(short) 255,(short) 255,(short) 255);
                    }else {
                        System.out.println(channel);
                        lampControl.pressPower(channel);
                        lampControl.changeRGB(lampControl.getChannel(), getR(),getG(),getB());
                    }
                }
            }
        });

        Spinner spinner = new Spinner(0, 10, 0);
        spinner.setOnMouseClicked(e -> {
            channel=(Integer) spinner.getValue();
        });

        Image img          = new Image("rsc/powerImage.jpg");
        ImageView view     = new ImageView(img);
        Button buttonOnOFF = new Button();
        buttonOnOFF.setGraphic(view);

        System.out.println("Channel: " + channel);
        System.out.println("State: "   + lampControl.getLampState());

        buttonOnOFF.setOnAction(actionEvent -> {
            if (lampControl.getLampState()==LampState.ON){
                System.out.println("Channel: " + channel);
                lampControl.pressPower(channel);
                lampControl.changeRGB(channel,(short) 255,(short) 255,(short) 255);
            }else {
                System.out.println("Channel: " + channel);
                lampControl.pressPower(channel);
                lampControl.changeRGB(lampControl.getChannel(), getR(),getG(),getB());
            }
            System.out.println("State: " + lampControl.getLampState());
        });


        Slider sliderR = new Slider(0, 255, 255);
        sliderR.setShowTickLabels(true);
        sliderR.setMajorTickUnit(50);
        sliderR.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number number, Number t1) {
                if (lampControl.getLampState()==LampState.ON) {
                    r = t1.shortValue();
                    labelRed.setText(String.valueOf("Red: " + t1.shortValue()));
                    lampControl.changeRGB(channel,r, g, b);
                }
            }
        });

        Slider sliderG = new Slider(0, 255, 255);
        sliderG.setShowTickLabels(true);
        sliderG.setMajorTickUnit(50);
        sliderG.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number number, Number t1) {
                if (lampControl.getLampState()==LampState.ON){
                    g = t1.shortValue();
                    labelGreen.setText(String.valueOf("Green: " + t1.shortValue()));
                    lampControl.changeRGB(channel,r,g,b);
                }
            }
        });

        Slider sliderB = new Slider(0, 255, 255);
        sliderB.setShowTickLabels(true);
        sliderB.setMajorTickUnit(50);
        sliderB.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number number, Number t1) {
                if (lampControl.getLampState()==LampState.ON){
                    b = t1.shortValue();
                    labelBlue.setText(String.valueOf("Blue: " + t1.shortValue()));
                    lampControl.changeRGB(channel,r,g,b);
                }
            }

        });

        setMargin(buttonOnOFF, new Insets(20, 20, 20, 20));
        setMargin(spinner, new Insets(10, 10, 10, 10));
        setMargin(sliderR, new Insets(10, 10, 10, 10));
        setMargin(sliderG, new Insets(10, 10, 10, 10));
        setMargin(sliderB, new Insets(10, 10, 10, 10));
        vbox.getChildren().addAll(buttonOnOFF, spinner,labelRed ,sliderR,labelGreen, sliderG, labelBlue, sliderB);
        getChildren().addAll(vbox);
    }

    public short getR(){
        return r;
    }

    public short getB() {
        return b;
    }

    public short getG() {
        return g;
    }
    private Label labelRed   = new Label("Red: 255");
    private Label labelGreen = new Label("Green: 255");
    private Label labelBlue  = new Label("Blue: 255");
    int channel;
    private short r = 255, g = 255, b = 255;
}