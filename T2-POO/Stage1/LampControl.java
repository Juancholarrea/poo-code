import javafx.scene.layout.Pane;

import javax.swing.plaf.nimbus.State;

public class LampControl {
    public LampControl(int channel, Cloud c){
        this.channel = channel;
        cloud = c;
        view  = new LampControlView(this);
    }
    public void pressPower(int channel){
        cloud.changeLampPowerState(channel);
    }
    public int getChannel(){
        return channel;
    }
    public LampState getLampState(){
        return cloud.getLampState();
    }
    public void changeRGB(int channel,short r, short g, short b){
        cloud.changeRGB(channel,r, g, b);
    }
    public Pane getView() { return view;}
    private int channel;
    private Cloud cloud;
    private Pane view;
}
