import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;

public class LampView extends Group {
    public LampView () {
        Polygon base = new Polygon();
        base.getPoints().addAll(36d, 40d,
                36d, 100d,
                26d, 100d,
                20d, 106d,
                20d, 120d,
                60d, 120d,
                60d, 106d,
                54d, 100d,
                44d, 100d,
                44d, 40d);
        base.setFill(Color.BLUEVIOLET);
        lampshade = new Polygon();
        lampshade.getPoints().addAll(20d, 0d, 0d, 40d, 80d, 40d, 60d, 0d);
        lampshade.setFill(Color.WHITE);
        getChildren().addAll(base,lampshade);
    }
    public void setColor(short r, short g, short b){
        Color c = Color.rgb(r,g,b);
        lampshade.setFill(c);
    }
    private Polygon lampshade;
}
