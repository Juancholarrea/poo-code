public abstract class DomoticDevice {
    public DomoticDevice(int channel){
        this.channel=channel;
    }
    public int getChannel() {
        return channel;
    }

    public void setChannel(int ch) { channel= ch;}
    private int channel;
}
